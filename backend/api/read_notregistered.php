<?php

/**
 * Returns the list of policies.
 */
require 'database.php';

$notuserlist = [];

$sql = "SELECT t.tuk_id, t.tuk_name, s.debcode, debtype, debname, debmob, packqty, r.ejamathno,
        CASE
            WHEN tiffinsts LIKE 'not taking' THEN 'Not Taking'
            ELSE 'Unregistered'
        END AS reg_status
        FROM tbl_scan s
        LEFT JOIN tbl_tuk t ON t.tuk_id = s.tuk_id
        LEFT JOIN tbl_registrations r ON r.debcode = s.debcode
        WHERE s.debcode
            IN (SELECT debcode
            FROM tbl_registrations
            WHERE confirmed = 0 OR tiffinsts NOT LIKE 'taking')";

if ($result = mysqli_query($con, $sql)) {
  $i = 0;
  while ($row = mysqli_fetch_assoc($result)) {
    $notuserlist[$i]['debcode']    = $row['debcode'];
    $notuserlist[$i]['debname']    = $row['debname'];
    $notuserlist[$i]['debtype']    = $row['debtype'];
    $notuserlist[$i]['debmob']     = $row['debmob'];
    $notuserlist[$i]['tuk_id']     = $row['tuk_id'];
    $notuserlist[$i]['ejamathno']  = $row['ejamathno'];
    $i++;
  }

  echo json_encode($notuserlist);
} else {
  http_response_code(404);
}
