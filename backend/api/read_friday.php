<?php

/**
 * Returns the list of policies.
 */
require 'database.php';

$frilist = [];
$sql = "SELECT debcode, debtype, debname, debmob, ejamathno, tuk_id FROM tbl_registrations
        WHERE tiffinfreq LIKE 'F'";

if ($result = mysqli_query($con, $sql)) {
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $frilist[$i]['debcode']        = $row['debcode'];
        $frilist[$i]['debname']        = $row['debname'];
        $frilist[$i]['debtype']        = $row['debtype'];
        $frilist[$i]['debmob']         = $row['debmob'];
        $frilist[$i]['tuk_id']         = $row['tuk_id'];
        $frilist[$i]['ejamathno']      = $row['ejamathno'];
        $i++;
    }

    echo json_encode($frilist);
} else {
    http_response_code(404);
}
