import { Component, OnInit, ViewChild } from '@angular/core';
import { Tuk } from '../tuk';
import { ApiService } from '../api.service';
import { FormControl, NgForm, FormBuilder } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { DialogBoxComponent } from '../dialog-box/dialog-box.component';
import { MatDialog } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';

@Component({
  selector: 'app-tuks',
  templateUrl: './tuks.component.html',
  styleUrls: ['./tuks.component.scss']
})
export class TuksComponent implements OnInit {

  tukForm;
  tableColumns: string[] = ['tuk_id', 'tuk_name', 'tuk_contact', 'action'];
  dataSource = [];

  constructor(private apiService: ApiService,
    private formBuilder: FormBuilder,
    private router: Router,
    private cookieService: CookieService,
    public dialog: MatDialog,
  ) {
    this.tukForm = this.formBuilder.group({
      tuk_name: '',
      address: '',
      email: '',
      mobile: ''
    });
  }

  openDialog(action, obj) {
    obj.action = action;
    console.log(obj);
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '300px',
      data: obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.event == 'Update') {
        this.updateRowData(result.data);
      } else if (result.event == 'Delete') {
        this.deleteRowData(result.data);
      }
    });
  }

  updateRowData(row_obj) {
    console.log(row_obj);
    this.dataSource = this.dataSource.filter((value, key) => {
      if (value.tuk_id == row_obj.tuk_id) {
        value.tuk_name = row_obj.tuk_name;
      }

      this.apiService.editTuk(row_obj).subscribe((tukList: Tuk[]) => {
        this.tukList = tukList;
        console.log(this.tukList);
      })

      this.apiService.readTuks().subscribe((tukList: Tuk[]) => {
        this.tukList = tukList;
        this.dataSource = tukList;
        console.log("DATA SOURCE", this.dataSource);
      })

      return true
    });
  }

  deleteRowData(row_obj) {
    this.dataSource = this.dataSource.filter((value, key) => {
      return value.tuk_id != row_obj.tuk_id;
    });

    this.apiService.deleteTuk(row_obj).subscribe((tukList: Tuk[]) => {
      this.tukList = tukList;
      console.log(this.tukList);
    })

    this.apiService.readTuks().subscribe((tukList: Tuk[]) => {
      this.tukList = tukList;
      this.dataSource = tukList;
      console.log("DATA SOURCE", this.dataSource);
    })
  }

  today = Math.floor(new Date().getTime() / 1000.0)
  dateTod = new Date().toISOString()
  serializedDate = new FormControl(this.dateTod);

  events: string[] = [];

  addEvent(type: string, event: MatDatepickerInputEvent<Date>) {
    this.events.push(`${type}: ${event.value}`);
    // console.log(event.value);

    this.today = Math.floor(new Date(event.value).getTime() / 1000.0)
  }

  //All API called arraylists
  tukList: Tuk[];
  tuk_data: Tuk[];

  ngOnInit(): void {

    var cookie = this.cookieService.get('login_user');
    console.log('Cookie Value', cookie);

    if (cookie != "") {

      this.apiService.readTuks().subscribe((tukList: Tuk[]) => {
        this.tukList = tukList;
        this.dataSource = tukList;
        console.log("DATA SOURCE", this.dataSource);
      })
    } else {
      this.router.navigate(['']);
    }


  }

  onSubmit(formData) {
    this.apiService.addTuk(formData).subscribe((tukList: Tuk[]) => {
      this.tukList = tukList;
      console.log(this.tukList);
    })

    this.apiService.readTuks().subscribe((tukList: Tuk[]) => {
      this.tukList = tukList;
      this.dataSource = tukList;
      console.log("DATA SOURCE", this.dataSource);
    })

    this.tukForm.reset();

    console.log(formData);
  }

  logout() {
    this.cookieService.deleteAll();
    this.router.navigate(['']);
  }

}
